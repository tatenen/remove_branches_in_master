#!/usr/bin/env php
<?php
date_default_timezone_set('Europe/Moscow');
$command = 'git branch -r |grep feature/';

/** @var array $output1 */
exec($command, $output1, $return);

$total = ['in_master' =>0, 'not_in_master' => 0];
$in_master = [];

foreach($output1 as $branch){
	$branch = trim($branch);
	$localBranch = trim(str_replace('origin/', '', $branch));

//	$command = "git checkout $localBranch ";
//	echo $command, PHP_EOL;
//	exec($command, $output, $return);

	$command = "git log --format=%B -n 1 $localBranch";
	echo $command, PHP_EOL;
	exec($command, $output, $return);
	$commitMessage = $output[0];
	unset($output);


	$command = "git log | grep '$commitMessage' ";
	echo $command, PHP_EOL;
	exec($command, $output, $return);
	$o = $output;
	unset($output);

	if($o[0] == 'grep: illegal byte sequence'){
		die('ere');
	}

	if($o){
		echo $localBranch . ' in master', PHP_EOL;
		$total['in_master']++;
		$in_master[] = $localBranch;
	} else {
		$total['not_in_master']++;
		echo $localBranch . ' not in master', PHP_EOL;
		continue;
	}
}

$date = new DateTime('2015-06-01');
foreach($in_master as $k => $b){
	$command = "git show -s --format=%ci $b";
//	echo $command, PHP_EOL;
	exec($command, $output, $return);

	$a = new DateTime($output[0]);
	unset($output);

//	echo $a->format('Y-m-d H:i:s'), PHP_EOL;
	if($a > $date){
		unset($in_master[$k]);
	}

//	echo $a->format('c'), PHP_EOL;

}



foreach($in_master as $b){
	$command = "git push origin :$b";
	echo $command, PHP_EOL;
	exec($command, $output, $return);
	unset($output);
}
$command = '';
var_dump($output);
var_dump($in_master);
var_dump($total);

echo 'before 2014 ' . count($in_master), PHP_EOL;

